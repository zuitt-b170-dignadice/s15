// s15 activity

console.log('Hello World')

/* 
- If the total of the two numbers is less than 10, add the numbers
- If the total of the two numbers is 10 - 20, subtract the numbers
- If the total of the two numbers is 21 - 29 multiply the numbers
- If the total of the two numbers is greater than or equal to 30, divide the numbers
*/

const num1 = parseInt(prompt('Enter first number'));
const num2 = parseInt(prompt('Enter second number'));
const sum = num1 + num2;
const diff = num1 - num2;
const prod = num1 * num2;
const quo = num1 / num2;


/* 
x + y < 10 then X + y
x + y => 10 || =< 20 then x - y
x + y => 21 || =< 29 then x * y 
x + y =< 30 then x / y 

*/

let answer = "";

function arit_op(sum){

    if (sum<10){
        return `Your number is ${sum}`
    }
    
    else if ( sum>= 10 || sum <= 20 ){
        return `Your number is ${diff}`
    }
    
    else if (sum >= 21 || sum <= 29){
        return `Your number is ${prod}`
    }
    
    else if (sum >= 30){
        return `Your number is ${quo}`
    }

    else{
        return `no number`
    }
}

answer = arit_op(sum);
console.log(answer);
alert(answer);

/* 

Use an alert for the total of 10 or greater and a 
console warning for the total of 9 or less.

*/


function alert_warn(sum){
    if (sum >= 10){
        alert(`Your Number is equal or greater than 10`)
    }
    if (sum <= 9){
        console.warn(`Your number is equal or less than 9`)
    }
};

out_alert_warn = alert_warn(sum);

/* 
Prompt the user for their name and age and print out different alert messages based on the user input:
-  If the name OR age is blank/null, print the message are you a time traveler?
-  If the name AND age is not blank, print the message with the user’s name and age.
*/

/* const yourName = prompt('What is your name?');
const yourAge = parseInt(prompt("What is your age?")); 
const false_require = yourName && yourAge;
const true_require = yourName || yourAge; */

/* case_1 = x_name || y_age;
case_2 = x_name && y_age; */
 


const [x_name, y_age] = [prompt('What is your name?'), parseInt(prompt(`What is your age?`))];

if (x_name === "" || x_name === null || age > 120 || age === null ){
    alert("Are your a time traveler?");
} else{
    alert(`Hello ${x_name}, your age is ${y_age}`);
};

/* function require_case(){
    if (x_name || y_age){
        alert(`Hello ${x_name}, you are ${y_age} years old`);
        console.log(`Hello ${x_name}, you are ${y_age} years old`);
    }

    if (x_name === undefined && y_age === undefined){
        alert('Are you a time traveller?');
        console.log('Are you a time traveller?');
    }
}; */



/* 

y_age >= 18 - you are of legal age
y_age <= 17 - You are not allowed here

*/

function isLegalAge(){
    return 'Your are of legal age';
}

function isUnderAge(){
    return 'You are not allowed here';
}

let legalAge = (y_age >= 18) ? isLegalAge() : isUnderAge();
alert(
 `${legalAge}`
);

switch(y_age){
    case 18 :
        console.log(`You are now allowed to party.`);
        break;
    case 21 :
        console.log(`You are now part of the adult society`);
        break;
    case 65 :
        console.log(`Thank you for your contribution to society`);
        break;
    default :
        console.log(`Are you sure you're not an alien?`);
};

function i_legalAge(y_age){
    try{
        alert(legalAge(y_age >= 18))
    }
    catch(error){
        console.warn("Age error")
    }
    finally{
        alert('this will show if your are legal or not')
    }
    
};

i_legalAge(y_age);
 




























