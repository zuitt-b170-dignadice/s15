// Mathematical Operators

/* function mod(){
    return 9+2
};

console.log(mod()); */

/* function mod(){
    return 9-2
};

console.log(mod()); */


/* function mod(){
    return 9*2
};

console.log(mod());
 */



// call it '()' to return

/* 

+ sum 
- difference
* product
/ quotient


*/

/* 

used when the calue of the first nnumber is stored in a variable; permorms the operation as well as storing the variable
+=
-=
*=
/=
%= modulo = fetch the remainde of the value.

*/


function mod(){
    /* return 9/2  */
    let x = 10;
    // return x += 2;
    // return x -= 2;
    // return x *=2;
    // return x /=2;        
    // return 9%2;
    return x %= 2;

};

console.log(mod());

// Assignment Operator (=) is used for assigning values to variables

let x = 1;
let sum = 1;
// sum = sum + 1
sum +=1;
x -= 1;

console.log(sum);
console.log(x);

// Increment & Decrement
// Pre - increment - add 1 before assigning the value to the variable.

let z = 1;
// let z = (1) + 1;
let increment = ++z;

console.log(`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);

// Post-increment - adding 1 After assingning the value to the variable
increment = z++;
// let z = 1 + (1)
console.log(`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);

// https://www.w3schools.com/js/js_operators.asp

// Pre-decrement - subtracts 1 befre assigning of values (basing from the most recent value of z )

let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);

// Post-decrement - subtracting 1 after assigning of values.
decrement = z--
// z= 2 - (1)
console.log(`Result of pre-decrement: ${decrement}`);
console.log(`Result of pre-decrement: ${z}`);

// Comparison Operators 
// Equality Operator (==) - compares if the two values are equal.

let juan = "juan";
console.log(1 == 1);
console.log(0 == false); // 0 is open, 1 is closed
console.log(juan == "juan");


// Strict Equality Operator (===)
console.log(1 === 1);
// uses - navigating in the webpage course, course outline webpage.
/* 

if (course === course){
    course.html

if (data == course outline){
    courseOutline.html
}
*/

// javascript is not anymore concerned with other relative fuctions of the data, it only compares the true values as they are.
console.log(1 === true); // considered the case\
console.log(juan === 'Juan');

// inequality (!=)
console.log(1 != 2);
console.log(1 != 1);
console.log(juan != "Juan");

// strict inequality (!==)

console.log(0 !== false);

// Other Comparison Operators
/* 

> Greater than
< less than
>= Greater than or equal
<= less than or equal

*/




// Logical Operators

/* 

AND operator (&&) - returns true if all operands are true

A B Output
0 0 0 
0 1 0
1 0 0
1 1 1

OR operator (||) - returns true if one of the the operands are true

A B Output
0 0 0 
0 1 1
1 0 1
1 1 1





*/


// AND operator

let isLegalAge = true;
let isRegistered = true;

let allRequirementMet = isLegalAge && isRegistered;
console.log(`Result of logical AND operator: ${allRequirementMet}`); 


// OR operator
allRequirementMet = isLegalAge || isRegistered;
console.log(`Result of logical OR operator: ${allRequirementMet}`);



// Selection Control Structures
/* 

IF statements - executes a command if a specified condition is true.
    SYNTAX : 
        if (condition){
            statements/command
        };

*/


let num = -1;
if (num<0){
    console.log('Hello')
};


let value = 30;
if (value > 10 && value < 40){
    console.log('Welcome to Zuitt')
};


/* 

if-else statement - executes a command if the first condition returns false.

    SYNTAX : 
        if(condition){
            statement if true
        }
        else{
            statement if false
        };

*/


num = 100;
if (num>10){
    console.log('Number is greater than 10')
} 

else{
    console.log('Number is less than 10')
};


// prompt - dialog box that has input fields
// Alert - dialog box that has no input fields; used for warnings, announcements, etc.
// parseInt - converts number in string data type into numericaal data type. it only recognizes numbers and ingnores the alphabets (it returns Nan)(Not a Number)

/* 

num = prompt("Please input a number.");
if(num>59){
    console.log("Senior Age")
}
else{
    console.log("Invalid Age")
}; 

*/

/* 
num = prompt("Please input a number.");
if(num>59){
    alert('Senior Age')
    console.log(num)
}
else{
    alert("Invalid Age")
    console.log(num)
};

 */


/* num = parseInt(prompt("Please input a number.")); // convert string to numerical
if(num>59){
    alert('Senior Age')
    console.log(num)
}
else{
    alert("Invalid Age")
    console.log(num)
}; */

// NaN - Not a Number

// if-elseif-else statement - multiple separate, yet connected conditions

/* 

    if(condition){
        statement
    }
    else if(condition){
        statement
    }
    .
    .
    .
    .
    .
    else{
        statement
    };

*/

/* 

1. Quezon
2. Valenzuela
3. Pasig
4. Taguig

*/

/* let city = parseInt(prompt("Enter a number"));
if (city === 1){
    alert("Welcome to Quezon Ciy")
}

else if (city === 2){
    alert('Welcome to Valenzuela City')
}
else if (city === 3){
    alert('Welcome to Pasig City')
}
else if (city === 4){
    alert('Welcome to Taguig City')
}
else{
    alert('Invalid number')
};
 */

 let message = "";

function determineTyphoonIntensity(windspeed){
    if(windspeed < 30){
        return `Not a typhoon yet`
    }

    else if (windspeed <= 61){
        return `tropical depresssion detected`
    }
    else if (windspeed >= 52 && windspeed <= 88){
        return `Tropical Storm Deteced.`
    }
    else if (windspeed >= 89 && windspeed <= 117){
        return `Severe Tropical Storm Detected`
    }
    else{
        return `Typhopn Detected`
    }
    
}; 

message = determineTyphoonIntensity(70);
console.log(message);


/* 
    Ternary Operator - shorthanded if else statement
    SYNTAX : 
        (condition) ? ifTrue : ifFalse
*/

let ternaryResult = (1 < 18)? true : false;
console.log(`result of ternary operator: ${ternaryResult}`);

let personName;
function isOfLegalAge(){
    personName = 'John';
    return 'Your are of the age limit';
}

function isUnderAge(){
    personName = "Jane";
    return "You are under the legal age";
}

let age = parseInt(prompt('What is your age?'));

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert(`Result of Ternary Operator in function: ${legalAge} ${personName}`);
console.log(`Result of Ternary Operator in function: ${legalAge} ${personName}`); 


// Switch Statement - shorthand for if-elseif-else should there be a lot of conditions to b e met


/* 
SYNTAX 
    switch (expression/parameter){
        case value1: 
            statement/s;
            break;
        case value1: 
            statement/s;
            break;
        case value1: 
            statement/s;
            break;
        case value1: 
            statement/s;
            break;
        .
        .
        .
        default:
            statements;
    }
*/

let day = prompt('What day is it today?').toLowerCase();
switch(day){
    case "sunday" :
        alert('The color of the day is red');
        break;
    case "sonday" :
        alert('The color of the day is orange');
        break;
    case "tuesday" :
        alert('The color of the day is yellow');
        break;
    case "wednesday" :
        alert('The color of the day is Green');
        break;
    case "thursday" :
        alert('The color of the day is blue');
        break;
    case "friday" :
        alert('The color of the day is violet');
        break;
    case "saturday" :
        alert('The color of the day is Indigo');
        break;
    default:
        alert("Please input a valid day");
};


//  Try-cat-finally statement
/* 

Commonly used for error handling

*/

function showIntentsityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}
showIntentsityAlert(56);

/* 
https://www.w3schools.com/jsref/jsref_try_catch.asp

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch
 */

/* 
Activity]=p-['
 ]
*/

















